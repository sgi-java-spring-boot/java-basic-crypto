package com.sgiasia.javaspringboot.crypto;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;

import java.security.Key;

public class Jwt {
    public Key key;

    public String encodeJwt() throws JoseException {
        String data = "Hello World!";
        System.out.println("Before encoded: " + data);

        this.key = new AesKey(ByteUtil.randomBytes(16));
        System.out.println("The key is: ");
        System.out.println(this.key);

        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setPayload(data);
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        jwe.setKey(key);
        String serializedJwe = jwe.getCompactSerialization();
        System.out.println("After encoded: ");
        System.out.println(serializedJwe);
        return serializedJwe;
    }

    public void decodeJwt(String serializedJwe) throws JoseException {
        System.out.println("\nData to decode:");
        System.out.println(serializedJwe);
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setAlgorithmConstraints(new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT,
                KeyManagementAlgorithmIdentifiers.A128KW));
        jwe.setContentEncryptionAlgorithmConstraints(new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT,
                ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256));
        jwe.setKey(this.key);
        jwe.setCompactSerialization(serializedJwe);
        System.out.println("After decoded: " + jwe.getPayload());
    }
}
