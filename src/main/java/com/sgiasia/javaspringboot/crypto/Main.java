package com.sgiasia.javaspringboot.crypto;

public class Main {
    public static void main(String[] args) throws Exception {
        /*Encryption en = new Encryption();
        en.encryptDecryptAES();
        en.asymmetricEncryptionDecryption();*/

/*        Hashing hashing = new Hashing();
        hashing.testMd5HashCalculation();
        hashing.testMd5HashChangedAfterDataChange();
        hashing.testMd5HashCollision();

        hashing.testSha1HashCalculation();*/

        Jwt jwt = new Jwt();
        String result = jwt.encodeJwt();
        jwt.decodeJwt(result);
    }
}