package com.sgiasia.javaspringboot.crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Base64;

public class Encryption {
    public void encryptDecryptAES() throws Exception{
        System.out.println("\nAES 256 encryption demo:");

        String data = "Hello World!";
        System.out.println("Data to encrypt: "+data);

        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(256);
        // Generate the secret key specs.
        SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();
        //String key = new String(Base64.getEncoder().encode(raw));
        System.out.println("Secret key: "+Helper.bytesToHex(raw));

        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] bytes = cipher.doFinal(data.getBytes());
        String encrypted = Helper.bytesToHex(bytes);
        System.out.println("AES Encryption result: "+encrypted);
        String encryptedBase64 = new String(Base64.getEncoder().encode(bytes));
        System.out.println("AES Encryption result Base64: "+encryptedBase64);

        Cipher cipherDecrypt = Cipher.getInstance("AES");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decryptedBytes = cipherDecrypt.doFinal(bytes);
        String decryptedString = new String(decryptedBytes);

        System.out.println("Decryption result: "+decryptedString);
    }

    public void asymmetricEncryptionDecryption() throws Exception{
        System.out.println("\nRSA 2048 encryption demo:");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        InputStream is = this.getClass().getResourceAsStream("/vivek.pfx");
        String password = "password";
        keyStore.load(is, password.toCharArray());

        PrivateKey priv = (PrivateKey) keyStore.getKey("vivek", password.toCharArray());
        System.out.println(priv.toString());

        Cipher encrypt = Cipher.getInstance("RSA");
        encrypt.init(Cipher.ENCRYPT_MODE, priv);

        String data = "Hello World!";
        System.out.println("String to encrypt: "+data);
        byte[] bytes = encrypt.doFinal(data.getBytes());

        //String encrypted = new String(Base64.getEncoder().encode(bytes));
        System.out.println("Encryption result: "+Helper.bytesToHex(bytes));

        Cipher decrypt = Cipher.getInstance("RSA");
        decrypt.init(Cipher.DECRYPT_MODE, keyStore.getCertificate("vivek"));
        byte[] decryptedBytes = decrypt.doFinal(bytes);
        String decryptedString = new String(decryptedBytes);

        System.out.println("Decryption result: "+decryptedString);


    }
}
