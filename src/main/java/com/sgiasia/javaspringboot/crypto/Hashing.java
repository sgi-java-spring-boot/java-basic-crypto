package com.sgiasia.javaspringboot.crypto;


import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Hashing {
    private String data = "The Future of Map Making is HERE";


    public void testMd5HashCalculation() throws NoSuchAlgorithmException {
        System.out.println("\nMD5 hashing demo:");
        System.out.println("String to hash: "+data);

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] digest = messageDigest.digest(data.getBytes());

        System.out.println("Result: "+Helper.bytesToHex(digest));
    }

    public void testMd5HashChangedAfterDataChange() throws NoSuchAlgorithmException {
        System.out.println("\nMD5 result change demo:");
        System.out.println("String before change: "+data);
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] digest = messageDigest.digest(data.getBytes());
        System.out.println("Result before change: "+Helper.bytesToHex(digest));

        String data2 = data.replace('E', 'F');
        System.out.println("String after change: "+data2);
        byte[] digest2 = messageDigest.digest(data2.getBytes());
        System.out.println("Result before change: "+Helper.bytesToHex(digest2));
    }

    public void testSha1HashCalculation() throws NoSuchAlgorithmException {
        System.out.println("\nSHA1 hashing demo:");
        System.out.println("String to hash: "+data);
        MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
        byte[] digest = messageDigest.digest(data.getBytes());
        System.out.println("Result: "+Helper.bytesToHex(digest));
    }

    public void testMd5HashCollision() throws NoSuchAlgorithmException {
        // Example from here - https://crypto.stackexchange.com/a/15889
        System.out.println("\nMD5 collision demo:");
        String string1 = "4dc968ff0ee35c209572d4777b721587d36fa7b21bdc56b74a3dc0783e7b9518afbfa200a8284bf36e8e4b55b35f427593d849676da0d1555d8360fb5f07fea2";
        String string2 = "4dc968ff0ee35c209572d4777b721587d36fa7b21bdc56b74a3dc0783e7b9518afbfa202a8284bf36e8e4b55b35f427593d849676da0d1d55d8360fb5f07fea2";

        System.out.println(string1.equals(string2));

        byte[] data1 = DatatypeConverter.parseHexBinary(string1);
        byte[] data2 = DatatypeConverter.parseHexBinary(string2);

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] digest1 = messageDigest.digest(data1);
        byte[] digest2 = messageDigest.digest(data2);

        String result1 = Helper.bytesToHex(digest1);
        String result2 = Helper.bytesToHex(digest2);
        System.out.println(result1);
        System.out.println(result2);

        System.out.println(result1.equals(result2));
    }
}
